defmodule Revel.MixProject do
  use Mix.Project

  def project do
    [
      app: :revel,
      version: "0.7.0",
      compilers: [:rustler] ++ Mix.compilers(),
      rustler_crates: rustler_crates(),
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Revel.Application, []}
    ]
  end

  defp rustler_crates do
    [revel_scanner: [
        path: "native/revel/scanner",
        mode: rustc_mode(Mix.env)
    ]]
  end

  defp rustc_mode(:prod), do: :release
  defp rustc_mode(_), do: :debug
  
  
  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:distillery, "~> 2.0"},
      {:plug, "~> 1.7"},
      {:plug_cowboy, "~> 2.0"},
      {:xml_builder, "~> 2.0.0"},
      {:jason, "~> 1.1"},
      {:sqlitex, "~> 1.5"},
      {:mime, "~> 1.2"},
      {:configparser_ex, "~> 4.0"},
      {:vice, "~> 0.1.0"},
      {:doteki, "~> 1.1"},
      {:bucs, "~> 1.1"},
      {:rustler, "~> 0.21.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
