defmodule Revel.Database do
  require Logger
  use GenServer

  @moduledoc """
  GenServer module to handle database access and updating
  """

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: :database)
  end
  
  def handle_info(:scan, data_dir) do
    Logger.debug("Scan requested")
    scan_async(data_dir)
    {:noreply, data_dir}
  end

  def handle_info(:scan_complete, data_dir) do
    Logger.debug("Scan complete")
    schedule_scan()
    {:noreply, data_dir}
  end

  def get_user(username) do
    GenServer.call(:database, {:get_user, {username}})
  end

  def get_files_by_id(id) do
    GenServer.call(:database, {:select_from_entities_join_files_by_id, {id}})
  end

  def get_files_by_parent_id(parent_id) do
    GenServer.call(:database, {:select_from_entities_join_files_by_parent_id, {parent_id}})
  end

  def get_folders_by_id(id) do
    GenServer.call(:database, {:select_from_entities_join_folders_by_id, {id}})
  end

  def get_folders_by_parent_id(parent_id) do
    GenServer.call(:database, {:select_from_entities_join_folders_by_parent_id, {parent_id}})
  end

  def get_folders_with_root(id) do
    GenServer.call(:database, {:select_from_entities_join_folders_by_parent_id, {id}})
  end

  def create_user(username, properties) do
    GenServer.call(:database, {:create_user, {username, properties}})
  end

  def update_user(username, properties) do
    GenServer.call(:database, {:update_user, {username, properties}})
  end

  def delete_user(username) do
    GenServer.call(:database, {:delete_user, username})
  end

  def get_artists() do
    GenServer.call(:database, {:get_artists})
  end
  
  def handle_call({:get_user, {username}}, _from, data_dir) do
    {:ok, query} =
      Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
        Sqlitex.query(
          db,
          "SELECT username, password, admin_role, settings_role, download_role, playlist_role, cover_art_role, root from users where username like ?",
          bind: [username]
        )
      end)

    {:reply, query, data_dir}
  end

  def handle_call({:select_from_entities_join_files_by_id, {id}}, _from, data_dir) do
    {:ok, files} =
      Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
        Sqlitex.query(
          db,
          "SELECT * FROM entities e join files f on e.id = f.id where e.type='file' AND e.id = ?",
          bind: [id]
        )
      end)

    {:reply, files, data_dir}
  end

  def handle_call({:select_from_entities_join_files_by_parent_id, {parent_id}}, _from, data_dir) do
    {:ok, files} =
      Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
        Sqlitex.query(
          db,
          "SELECT * FROM entities e join files f on e.id = f.id where e.parent = ? ORDER BY f.trackn COLLATE NOCASE ASC",
          bind: [parent_id]
        )
      end)

    {:reply, files, data_dir}
  end

  def handle_call({:select_from_entities_join_folders_by_id, {id}}, _from, data_dir) do
    {:ok, folders} =
      Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
        Sqlitex.query(
          db,
          "SELECT * FROM entities e join folders f on e.id = f.id where e.id = ?",
          bind: [id]
        )
      end)

    {:reply, folders, data_dir}
  end

  def handle_call({:select_from_entities_join_folders_by_parent_id, {parent_id}}, _from, data_dir) do
    {:ok, folders} =
      Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
        Sqlitex.query(
          db,
          "SELECT * FROM entities e join folders f on e.id = f.id where e.parent = ?",
          bind: [parent_id]
        )
      end)

    {:reply, folders, data_dir}
  end

  def handle_call({:create_user, {username, properties}}, _from, data_dir) do
    # user table creation query
    # INSERT INTO users (username, \"password\", admin_role, settings_role, download_role, playlist_role, cover_art_role) VALUES ('admin', \"password\", 1, 1, 1, 1, 1);\
    case Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
           Sqlitex.query(
             db,
             "INSERT INTO users (username, password, admin_role, settings_role, download_role, playlist_role, cover_art_role) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
             bind: [
               username,
               properties[:password],
               properties[:admin_role],
               properties[:settings_role],
               properties[:download_role],
               properties[:playlist_role],
               properties[:cover_art_role]
             ]
           )
         end) do
      {:ok, []} ->
        Logger.info("Database: User " <> username <> " was created sucessfully")
        {:reply, :ok, data_dir}

      {:error, {:constraint, constraint}} ->
        Logger.warn(
          "Creating user \"" <>
            username <> "\" failed with error \"" <> List.to_string(constraint) <> "\""
        )

        {:reply, :error, data_dir}
    end
  end

  def handle_call({:update_user, {username, properties}}, _from, data_dir) do
    # user table creation query
    # INSERT INTO users (username, \"password\", admin_role, settings_role, download_role, playlist_role, cover_art_role) VALUES ('admin', \"password\", 1, 1, 1, 1, 1);\
    IO.puts(properties[:admin_role])

    case Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
           Sqlitex.query(
             db,
             "INSERT OR REPLACE INTO users (username, password, admin_role, settings_role, download_role, playlist_role, cover_art_role) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
             bind: [
               username,
               properties[:password],
               properties[:admin_role],
               properties[:settings_role],
               properties[:download_role],
               properties[:playlist_role],
               properties[:cover_art_role]
             ]
           )
         end) do
      {:ok, []} ->
        Logger.info("Database: User " <> username <> " was updated sucessfully")
        {:reply, :ok, data_dir}

      {:error, {:constraint, constraint}} ->
        Logger.warn(
          "Updating user \"" <>
            username <> "\" failed with error \"" <> List.to_string(constraint) <> "\""
        )

        {:reply, :error, data_dir}
    end
  end

  def handle_call({:delete_user, username}, _from, data_dir) do
    case Sqlitex.with_db(data_dir <> "/revel.db", fn db ->
           Sqlitex.query(
             db,
             "DELETE FROM users WHERE username like ?",
             bind: [username]
           )
         end) do
      {:ok, []} ->
        Logger.info("Database: User " <> username <> " was deleted sucessfully")
        {:reply, :ok, data_dir}

      {:error, {:constraint, constraint}} ->
        Logger.warn(
          "Deleting user \"" <>
            username <> "\" failed with error \"" <> List.to_string(constraint) <> "\""
        )
    end
  end

  def handle_call({:get_artists}, _from, data_dir) do
    Logger.info("get_artists called")
  end
  
  def init(_constant) do
    # Run revel indexer here

    xdg_data = System.get_env("XDG_DATA_HOME")
    home = System.get_env("HOME")

    cond do
      xdg_data != nil ->
        Logger.debug("Locating database using $XDG_DATA_HOME (" <> xdg_data <> ")")
        data_dir = Path.join([xdg_data, "revel"])
        first_scan(data_dir)
        schedule_scan()
        {:ok, data_dir}

      home != nil ->
        Logger.debug("Locating database using $HOME (" <> home <> ")")
        data_dir = Path.join([home, ".local/share", "revel"])
        first_scan(data_dir)
        schedule_scan()
        {:ok, data_dir}
    end
  end

  defp first_scan(data_dir) do
    database = data_dir <> "/revel.db"

    case File.stat(database) do
      {:ok, _stat} ->
        Logger.info("Updating database at " <> database <> ", this may take some time")
        :ok = scan(data_dir)
        Logger.info("Updated database " <> database <> " sucessfully")

      {:error, :enoent} ->
        case File.stat(data_dir) do
          {:error, :enoent} ->
            Logger.info("Creating data folder at " <> data_dir)
            File.mkdir_p(data_dir)

          {:error, error} ->
            raise error

          {:ok, _stat} ->
            nil
        end

        Logger.info("Creating database at " <> database <> ", this make take some time")

        scan_path = Revel.Config.get_val("Paths", "music_path")
        :ok = Revel.Scanner.create_database(database, scan_path, 4)

        Logger.info("Database created at " <> database)
    end
  end

  defp schedule_scan() do
    Process.send_after(self(), :scan, 600_000)
    # Default scanning interval is 1 hour
    # TODO: change this to pull from Revel.Config
    # Process.send_after(self(), :scan, 5000)
  end

  defp scan(data_dir) do
    database = data_dir <> "/revel.db"
    scan_path = Revel.Config.get_val("Paths", "music_path")
    :ok = Revel.Scanner.update_database(database, scan_path, 4)
  end

  defp scan_async(data_dir) do
    database = data_dir <> "/revel.db"
    Logger.info("Updating database at " <> database)

    Supervisor.start_link(
      [
        {Revel.Scanner, {self(), database}}
      ],
      strategy: :one_for_one
    )
  end
end
