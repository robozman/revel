First Run Configuration
=======================

Variables
---------

This tutorial will use a few variables throughout. Fill them in as you go
with your information.


- ``<server_ip>``: This is the IP of your server

  - If you are using docker, this is the ip we found in the docker installation
    tutorial.
  - If you are running from source, this defaults to ``127.0.0.1``.

- ``<admin_password>``: This in the password you want for your admin account.
- ``<username>``: This is the username you would like for your non admin user.
- ``<password>``: This is the password you would like for your non admin user.


Check If The Server Is Connectable
----------------------------------

To check if the server is connectable, run::

    $ python3 revel-config.py --url http://server_ip>:32320/ ping

If successful, you should see::

    <subsonic-response status="ok" version="1.15.0" xmlns="http://subsonic.org/restapi"/>


Changing The Default Admin Password
-----------------------------------

The default account has username ``admin`` and password ``password``.

We will change this password to something else. Note that due to the way the
subsonic API is implemented, this password is required to be stored in
plaintext. It also can potentially be transmitted by clients in plaintext when
communicating with the server. A randomly generated password that is used
nowhere else should be used here.

To change this password, run the following::

    $ python3 revel-config.py --url http://<server_ip>:32320/ \
        updateUser admin --target_password <admin_password>

.. note::
    
    Revel automatically trys using the default login credentials if none are
    specified. For the subsequent commands we will be specifying our new
    admin password each time using ``--password``.

Creating A Non-Admin User
-------------------------

Typically you will not want to use the admin user for your day to day usage.

To create a regular user, run the following::

    $ python3 revel-config.py --url http://<server_ip>:32320 \
        --password <admin_password> \
        createUser <username> <password>


