Installing Revel-Config
=======================

Downloading the Source
----------------------

To get the latest release, download the source from
https://gitlab.com/robozman/revel/-/releases.

To get the latest development code, clone the Revel source via git::

    $ git clone https://gitlab.com/robozman/revel.git


Installing Dependencies
-----------------------

Revel-Config is written in Python 3. Therefore it requires Python 3 to be
installed. Get Python 3 from a package repository near you.

A better python dependency management and installation strategy is in the works
for revel-config. In the meantime, installing requests should be sufficient.

This can be done by running the following::

    $ pip3 install --user requests


Running Revel-Config
--------------------

Revel-Config can be run by running the following::

    $ python3 revel-config.py --help

Check out the configuration page for more info on how to use Revel-Config to
configure your server.
